﻿using System.Net;

namespace EasyUDP
{
    public struct Message
    {
        public string value;
        public EndPoint sender;

        public Message(string value, EndPoint sender)
        {
            this.value = value;
            this.sender = sender;
        }
    }
}