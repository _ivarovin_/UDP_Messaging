﻿using System;
using System.Net;
using System.Net.Sockets;
using EasyUDP.Utilities;

namespace EasyUDP
{
    public class Server
    {
        public Action<Message> onMessageReceived;

        private readonly Socket socket;

        public Server()
        {
            socket = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);
            socket.Bind(new IPEndPoint(Utils.LocalIP, Utils.Port));
            socket.ReceiveTimeout = 50;
        }

        ~Server() => Close();

        public void Listen()
        {
            var buffer = new byte[1024];
            var senderEndPoint = new IPEndPoint(0, 0) as EndPoint;

            try
            {
                var messageLength = socket.ReceiveFrom(buffer, ref senderEndPoint);
                var message = Utils.GetString(buffer, messageLength);

                InterpretMessage(message, senderEndPoint);
            }
            catch { }
        }

        private void InterpretMessage(string message, EndPoint sender)
        {
            if (string.IsNullOrEmpty(message)) return;
            if (message.Equals("PING")) SendTo("PONG", sender);

            onMessageReceived?.Invoke(new Message(message, sender));
        }

        public void SendTo(string message, EndPoint sender)
        {
            socket.SendTo(Utils.GetBytes(message), sender);
        }

        public void Close() => socket.Close();
    }
}