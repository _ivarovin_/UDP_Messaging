﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Threading.Tasks;
using EasyUDP.Utilities;

namespace EasyUDP
{
    internal class IPFinder
    {
        private const string pingMessage = "PING";
        private const string expectedResponse = "PONG";
        private const int findDelayMillis = 100;

        private readonly byte[] buffer = new byte[1024];
        private readonly Socket socket;

        private EndPoint serverEndPoint;

        public IPFinder()
        {
            socket = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);
            socket.EnableBroadcast = true;
            socket.ReceiveTimeout = 50;
        }

        public async Task<string> FindServerIPAsync()
        {
            serverEndPoint = new IPEndPoint(0, 0);

            while (true)
            {
                if (TryFindServer()) break;

                await Task.Delay(findDelayMillis);
            }

            return ((IPEndPoint)serverEndPoint).Address.ToString();
        }

        private bool TryFindServer()
        {
            try
            {
                SendPing();

                return GetResponse().Equals(expectedResponse);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return false;
            }
        }

        private void SendPing()
        {
            var ping = Utils.GetBytes(pingMessage);
            var endPoint = new IPEndPoint(IPAddress.Broadcast, Utils.Port);

            socket.SendTo(ping, endPoint);
        }

        private string GetResponse()
        {
            var messageLength = socket.ReceiveFrom(buffer, ref serverEndPoint);

            return Utils.GetString(buffer, messageLength);
        }
    }
}