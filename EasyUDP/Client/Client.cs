﻿using System.Net;
using System.Net.Sockets;
using System.Threading.Tasks;
using EasyUDP.Utilities;

namespace EasyUDP
{
    public class Client
    {
        private readonly Socket socket;

        public Client()
        {
            socket = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);
        }

        ~Client() => Close();

        public async Task AutoConnect()
        {
            var serverIP = await new IPFinder().FindServerIPAsync();

            Connect(serverIP);
        }

        public void Connect(string ip)
        {
            var endPoint = new IPEndPoint(IPAddress.Parse(ip), Utils.Port);

            socket.Connect(endPoint);
        }

        public void Send(string message) => socket.Send(Utils.GetBytes(message));
        public void Close() => socket.Close();
    }
}